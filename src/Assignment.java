import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Assignment {
    private JButton curryButton;
    private JButton gyozaButton;
    private JTextPane textPane1;
    private JPanel root;
    private JButton humburgerButton;
    private JButton sushiButton;
    private JButton tendonButton;
    private JButton ramenButton;
    private JTextPane textPane2;
    private JButton checkOutButton;


    int total = 0;

    void order(String food, int yen) {
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + " ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0) {
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.");
            String currentText = textPane1.getText();
            textPane1.setText(currentText + food + "　　　" + yen + "yen\n");
            total += yen;
            textPane2.setText("Total   " + total + "yen");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Assignment");
        frame.setContentPane(new Assignment().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public Assignment() {
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Curry", 700);

            }
        });
        curryButton.setIcon(new ImageIcon(this.getClass().getResource("curry.jpg")));

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Gyoza", 900);
            }
        });
        gyozaButton.setIcon(new ImageIcon(this.getClass().getResource("gyoza.jpg")));

        humburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Humburger", 600);
            }
        });
        humburgerButton.setIcon(new ImageIcon(this.getClass().getResource("hamburger.jpg")));

        sushiButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sushi", 1000);
            }
        });
        sushiButton.setIcon(new ImageIcon(this.getClass().getResource("sushi.jpg")));

        tendonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tendon", 1100);
            }
        });
        tendonButton.setIcon(new ImageIcon(this.getClass().getResource("tendon.jpg")));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen", 800);
            }
        });
        ramenButton.setIcon(new ImageIcon(this.getClass().getResource("ramen.jpg")));


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkout = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Order check out",
                        JOptionPane.YES_NO_OPTION);

                if (checkout == 0) {
                        JOptionPane.showMessageDialog(null, "Thank you for ordering! The total price is " + total + "yen.\n"+
                              "Or, the total price is "+ Math.round(total /114 )+"$" + "It will be served as soon as possible.");
                }
                    total = 0;
                    textPane1.setText("");
                    textPane2.setText("Total");
                }

        });
    }
}
